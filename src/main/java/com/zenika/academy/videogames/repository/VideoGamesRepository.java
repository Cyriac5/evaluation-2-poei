package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class VideoGamesRepository {

    private HashMap<Long, VideoGame> videoGamesById = new HashMap<>();

    public List<VideoGame> getAll() {
        return List.copyOf(this.videoGamesById.values());
    }

    public Optional<VideoGame> get(Long id) {
        return Optional.ofNullable(this.videoGamesById.get(id));
    }

    public Optional<VideoGame> getByGenre( Genre genre){
        for(Map.Entry<Long, VideoGame> entry : this.videoGamesById.entrySet()){
            if(entry.getValue().getGenres().contains(genre)){
                return Optional.ofNullable(entry.getValue());
            }
        }
        return Optional.empty();
    }

    public void save(VideoGame v) {
        this.videoGamesById.put(v.getId(), v);
    }

    public void delete(VideoGame v){ this.videoGamesById.remove(v.getId(),v);}
}
