package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class VideoGamesService {

    private final VideoGamesRepository videoGamesRepository;

    @Autowired
    public VideoGamesService(VideoGamesRepository videoGamesRepository) {
        this.videoGamesRepository = videoGamesRepository;
    }

    public List<VideoGame> ownedVideoGames() {
        return this.videoGamesRepository.getAll();
    }

//    public List<VideoGame> gamesByGenre(Genre genre){
//        List<VideoGame> gamesList = new ArrayList<>();
//
//        for(VideoGame entry : videoGamesRepository.getAll()){
//            if (videoGamesRepository.getByGenre(genre).equals(genre){
//                gamesList.add(entry.get)
//
//            }
//
//
//            }
//        }


    public VideoGame getOneVideoGame(Long id) {
        return this.videoGamesRepository.get(id)
                .orElseThrow(() -> new RuntimeException("This video game doesn't exist"));
    }

    public VideoGame addVideoGame(String name) {

        RawgDatabaseClient client = new RawgDatabaseClient();
        VideoGame newGame = client.getVideoGameFromName(name);

        videoGamesRepository.save(newGame);

        return newGame;
    }

    public void deleteVideoGame(long id){
        VideoGame gameToDelete = videoGamesRepository.get(id)
                .orElseThrow(() -> new RuntimeException("This video game doesn't exist"));
        videoGamesRepository.delete(gameToDelete);
    }
}
